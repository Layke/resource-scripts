#!/usr/bin/env python
#
# Various functions to update genesets in ODE from their data sources
#
# ---
#
# Copyright (c) 2011 Jeremy Jay <jeremy@pbnjay.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys
import psycopg2

class OBOTreeLoader(object):
    def __init__(self, file):
        """ loads records from an OBO file """
        print >>sys.stderr, "Loading OBO source..."

        if isinstance(file, str):
            file=open(file)

        obodate=None
        current_record=None
        go_records=[]
        for line in file:
            line=line.strip()
            if line=='':
                continue

            if line[0]=='[' and line[-1]==']':
            #if line=='[Term]':
                if current_record is not None:
                    go_records.append(current_record)
                current_record=[]
            elif current_record is not None:
                current_record.append(line.split(': ',2))
            else:
                d=line.split(': ',2)
                if d[0]=='date':
                    obodate=d[1].strip()

        go_records.append(current_record)

        print >>sys.stderr, "   Loaded %d records..." % (len(go_records),)

        self.data = {}
        self.genemap = {}
        self.oldmap = {}

        for rec in go_records:
            godata = {
                'id': None,
                'name': None,
                'def': None,
                'parents': set(),
                'obodate': obodate,
            }
            oldnames=set()
            newnames=set()

            obsolete=False
            for data in rec:
                if data[0]=='id':
                    godata['id']=data[1].strip()
                elif data[0]=='name':
                    godata['name']=data[1].strip()
                elif data[0]=='def':
                    godata['def']=data[1].strip()

                elif data[0]=='is_a':
                    d2 = data[1].split("!",2)
                    godata['parents'].add(d2[0].strip())
                #elif data[0]=='relationship':
                #    d2 = data[1].strip().split(" ",3)
                #    godata['parents'].add(d2[1].strip())
                elif data[0]=='is_obsolete':
                    obsolete=True
                elif data[0]=='consider':
                    newnames.add(data[1].strip())
                elif data[0]=='alt_id':
                    oldnames.add(data[1].strip())

            if not obsolete:
                self.data[ godata['id'] ] = godata
                for oldid in oldnames:
                    if oldid not in self.oldmap:
                        self.oldmap[ oldid ]=set()
                    self.oldmap[ oldid ].update( [godata['id']] )
            else:
                if godata['id'] not in self.oldmap:
                    self.oldmap[ godata['id'] ]=set()
                self.oldmap[ godata['id'] ].update(newnames)

        for goid, info in self.data.items():
            info['ancestors'] = set([goid])
            tocheck = set([goid])
            seen = set()

            while len(tocheck)!=0:
                anc = tocheck.pop()
                if anc not in seen:
                    info['ancestors'].update( self.data[anc]['parents'] )
                    tocheck.update( self.data[anc]['parents'] )
                    seen.add(anc)

    def load_associations(self, file, gene_cd, id_cd, delim="\t", comments="!#;"):
        self.genemap={}

        if isinstance(file, str):
            file=open(file)

        print >>sys.stderr, "   Loading gene associations..."
        for line in file:
            if delim not in line or line[0] in comments:
                continue
            d=[x.strip() for x in line.split(delim)]
            ids=[ d[id_cd[0]] ]
            if id_cd[1] is not None:
                ids=[x.strip() for x in ids[0].split(gene_cd[1])]

            genes=[ d[gene_cd[0]] ]
            if gene_cd[1] is not None:
                genes=[x.strip() for x in genes[0].split(gene_cd[1])]

            mapids=set()
            for id in ids:
                mapids.update( self.data[id]['ancestors'] )

            for id in mapids:
                for gene in genes:
                    if id not in self.genemap:
                        self.genemap[id]=set()
                    self.genemap[id].add(gene)

        print >>sys.stderr, "   Done."


def update_fromOBO(obo_source, assoc_source, colinfo, fields, dbmap_sql):
    tree = OBOTreeLoader(obo_source)
    tree.load_associations(assoc_source, colinfo[0], colinfo[1])

    db_id_map = {}

    con = psycopg2.connect("host=ode-db1 dbname=ODE user=odeweb")
    cur = con.cursor()
    cur.execute("set search_path to production,extsrc,odestatic;")
    cur.execute(dbmap_sql)
    for row in cur:
        db_id_map[row[1].strip()]=row[0]

    for term,genes in tree.genemap.items():
        contents = "gene\tbinary\n" + "\t1\n".join(genes) + "\t1"
        FSQL = "INSERT INTO file (file_created,file_size,file_contents) VALUES (now(),%s,%s) RETURNING file_id;"
        cur.execute(FSQL, (len(contents), contents))
        file_id=cur.fetchone()[0]

        if term in db_id_map:
            SQL1 = '=%s, '.join(sorted(fields.keys()))+'=%s'
            SQL = 'UPDATE geneset SET file_id=%s, %s WHERE gs_id=%s;' % (file_id, SQL1, db_id_map[term])
            tup = tuple([fields[f] % tree.data[term] for f in sorted(fields.keys())])
            cur.execute(SQL, tup)
        else:
            SQL = 'INSERT INTO geneset (gs_created,gs_count,file_id,%s) VALUES (now(),0,%s%s) RETURNING gs_id;' % (','.join(sorted(fields.keys())), file_id, ',%s'*len(fields.keys()))
            tup = tuple([fields[f] % tree.data[term] for f in sorted(fields.keys())])
            cur.execute(SQL, tup)
            row = cur.fetchone()
            db_id_map[term]=row[0]

    todeprecate = set(tree.oldmap.keys()).intersection(db_id_map.keys())
    for term in todeprecate:
        other_gsids=[]
        for other in tree.oldmap[term]:
            if other in db_id_map:
                other_gsids.append(str(db_id_map[other]))

        if len(other_gsids)>0:
            status='deprecated:'+','.join(other_gsids)
        else:
            status='deleted'
        cur.execute( 'UPDATE geneset SET gs_status=\'%s\' WHERE gs_id=%s;' % (status, db_id_map[term]) )

    con.rollback()
