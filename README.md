ODE/GeneWeaver update scripts
=============================

This is a collection of scripts used to update the GeneWeaver database.
Initially, this consists of the Resource-grade Tier 1 GeneSets.

Gene Ontology and Mammalian Phenotype OBO-format files are fetched from the
OBOFoundry CVS server. Mouse gene associations for each are fetched from MGI
directly (other GO species will be forthcoming).

