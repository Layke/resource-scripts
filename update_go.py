#!/usr/bin/env python
#
# updates gene ontology sets in-place from the source data
#

OBO_URL = 'http://obo.cvs.sourceforge.net/viewvc/obo/obo/ontology/genomic-proteomic/gene_ontology_edit.obo'
ASSOC_URL = 'ftp://ftp.informatics.jax.org/pub/reports/gene_association.mgi'
ASSOC_GENE = (1,None)
ASSOC_TERM = (4,None)

fields = {
    'gs_name': "%(id)s %(name)s",
    'gs_description': "%(def)s Data derived from gene_association.mgi and OBO tree dated %(obodate)s",
    'gs_abbreviation': "%(name)s (GO)",
    'gs_uri': "http://www.informatics.jax.org/searches/accession_report.cgi?id=%(id)s",
    'gs_gene_id_type': '-10',
    'sp_id': '1',
    'cur_id': '1',
    'usr_id': '490',
    'gs_groups': '0',
}

SQL = "select gs_id,substring(gs_name from 0 for 11) from geneset where gs_name like 'GO:%' and gs_status not like 'de%';"

########

import urllib2
from ode_updater import update_fromOBO
obo_source = urllib2.urlopen(OBO_URL)
assoc_source = urllib2.urlopen(ASSOC_URL)
update_fromOBO(obo_source, assoc_source, (ASSOC_GENE,ASSOC_TERM), fields, SQL)

